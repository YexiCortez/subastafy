<?php
/**
 * Displays footer site info
 *
 * @package Travel Hub
 * @subpackage travel_hub
 */

?>
<div class="site-info">
	<div class="container">
		<p><?php echo esc_html(get_theme_mod('adventure_travelling_footer_text',__('Travel WordPress Theme By ThemesPride','travel-hub'))); ?> <?php travel_hub_credit(); ?></p>
	</div>
</div>